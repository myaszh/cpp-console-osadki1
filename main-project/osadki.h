#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int day;
    int month;
};

struct kol
{
    int kol[MAX_STRING_SIZE];
};

struct osadki_opisanie
{
    char title[MAX_STRING_SIZE];
};

#endif